# Alter Ego v2.4.2 Source

This repository is for the Alter Ego 2.4.2 source, and will have branches for things such as English translations, 16:9 screen ratios, and more.


## Dependencies 

The list of dependencies are as followed.
[Unity Alpha. Keep this regularly updated if you are going to either follow development, or commit changes.](https://unity3d.com/alpha/2020.1a)
Latest version is 2020.1.0.a24.

Install Visual Studio when installing the Alpha Version. 
I personally use NotePad++ as my IDE, but you can use whatever works for you.

## Committing to the git

When commiting, make sure to push to a seperate branch, and request a merge. 